/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  Alert,
  ImageBackground.
  TextInput,
  ScrollView,
  Platform,
} from 'react-native';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: 'Jorge'
    }
  }
  render() {
    const version = Platform.Version;
    return (
        <ImageBackground
            source={require('./assets/bgimg.jpg')}
            style={styles.container}>
          <ScrollView>
            <View style={styles.header}>
              <View style={styles.headerLeft}>
                <Image
                    source={require('./assets/pilots.jpg')}
                    style={styles.logo}
                />
              </View>
              <View style={styles.headerRight}>
                <Button title="Login" onPress={this.saludar} />
              </View>
            </View>
            <View style={styles.body}>
              {Platform.OS === 'android' ? (
                  <Text>Hola Ciapfa en Android! {version}</Text>
              ) : (
                  <Text>Hola Ciapfa en iOS! {version}</Text>
              )}
              <TextInput
                  placeholder="Nombre de usuario"
                  placeholderTextColor="white"
                  maxLength={10}
                  style={{
                    borderWidth: 1,
                    borderColor: 'white',
                    padding: 5,
                    marginTop: 5,
                  }}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
              <Image
                  source={require('./assets/pilots.jpg')}
                  style={styles.logo}
              />
            </View>
          </ScrollView>
        </ImageBackground>
    );
  }

  saludar = () => {
    Alert.alert('Botón presionado', 'Hola!');
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    /*flexDirection: 'column',*/
    ...Platform.select({
      ios: {
        flexDirection: 'row',
      },
      android: {
        flexDirection: 'column',
      },
    }),
  },
  header: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: 15,
    marginLeft: 15,
  },
  headerRight: {
    flex: 1,
    marginRight: 10,
  },
  headerLeft: {
    flex: 3,
  },
  logo: {
    width: 70,
    height: 70,
    borderRadius: 5,
    resizeMode: 'contain',
  },
  body: {
    flex: 1,
    alignItems: 'center',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default App;
